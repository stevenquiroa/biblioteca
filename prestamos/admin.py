from django.contrib import admin
from .models import Sede, Copy, Prestamo

# Register your models here.
class SedeAdmin(admin.ModelAdmin):
    list_display = ('name', 'location')
    # list_filter = ['sede']
    search_fields = ['name']

admin.site.register(Sede, SedeAdmin)

class CopyAdmin(admin.ModelAdmin):
    list_display = ('book', 'status')
    list_filter = ['sede', 'status']
    search_fields = ['book__title']

admin.site.register(Copy, CopyAdmin)

class PrestamoAdmin(admin.ModelAdmin):
    list_display = ('person', 'copy', 'prestamo_date', 'devolucion_prevista_date', 'devolucion_real_date')
    list_filter = ['copy__book__categories__name']
    search_fields = ['person__first_name', 'person__last_name']
admin.site.register(Prestamo, PrestamoAdmin)