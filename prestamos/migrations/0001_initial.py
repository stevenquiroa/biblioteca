# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('libros', '0001_initial'),
        ('estudiantes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Copy',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.IntegerField()),
                ('book', models.ForeignKey(to='libros.Book')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Prestamo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('prestamo_date', models.DateField()),
                ('devolucion_prevista_date', models.DateField()),
                ('devolucion_real_date', models.DateField(null=True, blank=True)),
                ('copy', models.ForeignKey(to='prestamos.Copy')),
                ('person', models.ForeignKey(to='estudiantes.Person')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sede',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('location', models.CharField(max_length=300)),
                ('maps', models.CharField(max_length=300)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='copy',
            name='sede',
            field=models.ForeignKey(to='prestamos.Sede'),
            preserve_default=True,
        ),
    ]
