from django.db import models
from estudiantes.models import Person
from libros.models import Book

# Create your models here.
class Sede(models.Model):
	name = models.CharField(max_length=200)
	location = models.CharField(max_length=300)
	maps = models.CharField(max_length=300)

	def __str__(self):
   		return self.name

class Copy(models.Model):
	STATUS_CHOICES = (
		(1, 'Disponible'),
		(2, 'No disponible'),
		(3, 'Prestado')
	)
	book = models.ForeignKey(Book)
	sede = models.ForeignKey(Sede)
	status = models.IntegerField(choices=STATUS_CHOICES) 
	def __str__(self):
		return str(self.book) + " - " + str(self.id)

class Prestamo(models.Model):
	person = models.ForeignKey(Person)
	copy = models.ForeignKey(Copy)
	prestamo_date = models.DateField()
	devolucion_prevista_date = models.DateField()
	devolucion_real_date = models.DateField(null=True, blank=True)
	
	def __str__(self):
		return str(self.person) + " - " + str(self.copy)