from django.db import models

# Create your models here.
class Author(models.Model):
	name = models.CharField(max_length=200)
	
	def __str__(self):
   		return self.name

class Category(models.Model):
	name = models.CharField(max_length=200)
	
	def __str__(self):
		return self.name

class Type(models.Model):
	name = models.CharField(max_length=200)
	
	def __str__(self):
		return self.name

class Book(models.Model):
	title = models.CharField(max_length=200)
	editorial = models.CharField(max_length=200)
	year = models.IntegerField()
	description = models.TextField()
	type = models.ForeignKey(Type)
	categories = models.ManyToManyField(Category)
	authors = models.ManyToManyField(Author)
	
	def __str__(self):
		return self.title
