from django.contrib import admin

# Register your models here.
from .models import Author, Category, Type, Book

admin.site.register(Author)
admin.site.register(Type)
admin.site.register(Category)
class BookAdmin(admin.ModelAdmin):
    list_display = ('title', 'editorial', 'type', 'year')
    list_filter = ['editorial', 'type', 'categories', 'authors', 'year']
    search_fields = ['title', 'editorial']
admin.site.register(Book, BookAdmin)