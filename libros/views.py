from django.shortcuts import render
from .models import Book
# Create your views here.

def index(request):
    latest_question_list = Book.objects.all()[:5]
    context = {'latest_books_list': latest_question_list}
    return render(request, 'books/index.html', context)
