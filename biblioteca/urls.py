from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'biblioteca.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^libros/', include('libros.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
