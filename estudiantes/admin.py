from django.contrib import admin
from .models import Facultad, Carrera, Role, Person
# Register your models here.
admin.site.register(Facultad)
admin.site.register(Carrera)

class RoleAdmin(admin.ModelAdmin):
    list_display = ('name', 'getteable_books')
    search_fields = ['name']
admin.site.register(Role, RoleAdmin)

class PersonAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'email', 'role', 'carrera')
    list_filter = ['carrera', 'role']
    search_fields = ['email', 'first_name']
admin.site.register(Person, PersonAdmin)
