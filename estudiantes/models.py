from django.db import models

# Create your models here.
# Create your models here.
class Facultad(models.Model):
	name = models.CharField(max_length=200)
	
	def __str__(self):
   		return self.name

class Carrera(models.Model):
	name = models.CharField(max_length=200)
	facultad = models.ForeignKey(Facultad)

	def __str__(self):
		return self.name

class Role(models.Model):
	name = models.CharField(max_length=200)
	getteable_books = models.IntegerField()

	def __str__(self):
		return self.name

class Person(models.Model):
	first_name = models.CharField(max_length=200)
	last_name = models.CharField(max_length=200)
	birthday = models.DateField()
	email = models.CharField(max_length=200)
	role = models.ForeignKey(Role)
	carrera = models.ForeignKey(Carrera)

	def __str__(self):
		return self.first_name + " " + self.last_name
